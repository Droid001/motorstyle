import { Router } from 'express';
import UsersController from '../controllers/users.controller';
import { CreateUserDto } from '../dtos/users.dto';
import Route from '../interfaces/routes.interface';
import validationMiddleware from '../middlewares/validation.middleware';
import adminMiddleware from '../middlewares/admin.middleware';

class UsersRoute implements Route {
  public path = '/users';
  public router = Router();
  public usersController = new UsersController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/get`, adminMiddleware, this.usersController.getUsers);
    //this.router.get(`${this.path}/:id`, this.usersController.getUserById);
    this.router.post(`${this.path}`, adminMiddleware, validationMiddleware(CreateUserDto), this.usersController.createUser);
    //this.router.put(`${this.path}/:id`, validationMiddleware(CreateUserDto, true), this.usersController.updateUser);
    this.router.delete(`${this.path}/:id`, adminMiddleware, this.usersController.deleteUser);
  }
}

export default UsersRoute;
