import store from "@/store";

export default async function admin(to, from, next) {
    await store.dispatch("CHECK");
    const role = store.getters.GET_ROLE;
    if (role == 'admin') {
        next()
    } else {
        role == 'guest' ? next('/login') : next('/')
    }
}