import * as mongoose from 'mongoose';
import { User } from '../interfaces/users.interface';

const userSchema = new mongoose.Schema({
  login:String,
  email: String,
  password: String,
  name: String,
  role: String,
  rememberToken:String
});

const userModel = mongoose.model<User & mongoose.Document>('User', userSchema);

export default userModel;
