import store from "@/store";

export default async function guest(to, from, next) {
    const role = store.getters.GET_ROLE;
    if (role == 'guest') {
        next();
        return;
    }
    await store.dispatch("CHECK");

    if (role == 'guest') {
        next()
    } else {
        role == 'user' ? next('/') : next('/admin')
    }
}