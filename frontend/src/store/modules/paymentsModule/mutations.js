export function SET_BALANCE(state, balance) {
  state.balance = balance;
}

export function SET_TRANSACTIONS(state, transactions) {
  state.transactionsList = transactions.transactionsList;
  state.isLastPage = transactions.isLastPage;
}

export function SET_TRANSACTION_STATUS(state, data) {
  state.transactionsList.forEach(tr => {
    if (tr.transactionNumber == data.transactionNumber) {
      tr.statusCode = data.statusCode;
      tr.status = data.status;
      console.log(tr.transactionNumber);
      return;
    }
  });
}
