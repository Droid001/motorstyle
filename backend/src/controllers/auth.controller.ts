import { NextFunction, Request, Response } from 'express';
import { CreateUserDto, CreateAuthUserDto } from '../dtos/users.dto';
import AuthService from '../services/auth.service';
import { User } from '../interfaces/users.interface';
import { RequestWithUser } from '../interfaces/auth.interface';

class AuthController {
  public authService = new AuthService();

  public signUp = async (req: Request, res: Response, next: NextFunction) => {
    const userData: CreateUserDto = req.body;

    try {
      const signUpUserData: User = await this.authService.signup(userData);
      res.status(201).json({ data: signUpUserData, message: 'signup' });
    } catch (error) {
      next(error);
    }
  }

  public logIn = async (req: Request, res: Response, next: NextFunction) => {
    const userData: CreateAuthUserDto = req.body;

    try {
      const { token, role, name } = await this.authService.login(userData);
      res.status(200).json({ token: token, role: role, name:name });
    } catch (error) {
      next(error);
    }
  }

  public logOut = async (req: Request, res: Response, next: NextFunction) => {

    try {
      await this.authService.logout(req.body.token);
      res.status(200).json({ message: 'logged out' });
    } catch (error) {
      next(error);
    }
  }
  public check = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const role: string = await this.authService.check(req.body.token);
      res.json({ role: role });
    }
    catch (error) {
      next(error);
    }
  }
}

export default AuthController;
