export interface Transaction {
  _id?: string;
  user:string
  transactionNumber: number,
  summ: number,
  accountNumber: number,
  status:string,
  statusCode:number
  date: number
}
