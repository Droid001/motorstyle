export function SET_AUTH_ROLE(state, role) {
    state.user.role = role;
}

export function SET_AUTH_DATA(state, data) {
    state.user.token = data.token;
    state.user.role = data.role;
    state.user.name = data.name;
}

export function SET_USERS(state, users) {
    state.users = users;
}