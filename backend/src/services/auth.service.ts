import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { CreateUserDto, CreateAuthUserDto } from '../dtos/users.dto';
import HttpException from '../exceptions/HttpException';
import { DataStoredInToken, TokenData } from '../interfaces/auth.interface';
import { User } from '../interfaces/users.interface';
import userModel from '../models/users.model';
import { isEmptyObject } from '../utils/util';

class AuthService {
  public users = userModel;

  public async signup(userData: CreateUserDto): Promise<User> {
    if (isEmptyObject(userData)) throw new HttpException(400, "You're not userData");

    const findUser: User = await this.users.findOne({ email: userData.email });
    if (findUser) throw new HttpException(409, `You're email ${userData.email} already exists`);

    const hashedPassword = await bcrypt.hash(userData.password, 10);
    const createUserData: User = await this.users.create({ ...userData, password: hashedPassword });

    return createUserData;
  }

  public async login(userData: CreateAuthUserDto): Promise<{ token: string, role: string, name: string }> {
    //проверка данных пользователя
    if (isEmptyObject(userData)) throw new HttpException(400, "You're not userData");
    //находим пользователя по логину
    const findUser: User = await this.users.findOne({ login: userData.login });
    if (!findUser) throw new HttpException(409, `You're login ${userData.login} not found`);
    //проверяем правильность пароля
    const isPasswordMatching: boolean = await bcrypt.compare(userData.password, findUser.password);
    if (!isPasswordMatching) throw new HttpException(409, "You're password not matching");
    //Создаем ключ для создания JWT
    const rememberToken = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    //сохраняем ключ
    await this.users.findOneAndUpdate({ login: userData.login }, { rememberToken: rememberToken });
    //Создаем jwt по ключу
    const token = this.createToken(rememberToken);

    return { token: token, role: findUser.role, name: findUser.name };
  }

  public async check(token: string):Promise<string> {
    const secret = process.env.JWT_SECRET;
    try {
      //дешифруем полученный jwt
      const json: any = jwt.verify(token, secret) as string;
      //находим пользователя по токену
      const findUser = await userModel.findOne({ rememberToken: json.rememberToken });
      //сравниваем токен из бд и расшиврованный из jwt
      if (findUser) {
        return findUser.role;
      }
        return 'guest'
    }
    catch (e) {
      return 'guest';
    }
  }

  public async logout(token: string): Promise<void> {
    const secret = process.env.JWT_SECRET;
    try {
      //дешифруем полученный jwt
      const json: any = jwt.verify(token, secret) as string;
      //удаляем ключ
      await this.users.findOneAndUpdate({ rememberToken: json.rememberToken }, { rememberToken: '' });
    }
    catch (e) {
      throw new HttpException(409, "User not found");
    }
  }

  public createToken(rememberToken: string): string {
    const dataStoredInToken: DataStoredInToken = { rememberToken: rememberToken };
    const secret: string = process.env.JWT_SECRET;
    const expiresIn: number = 60 * 60;

    return jwt.sign(dataStoredInToken, secret, { expiresIn });
  }
}

export default AuthService;
