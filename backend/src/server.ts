import 'dotenv/config';
import App from './app';
import IndexRoute from './routes/index.route';
import UsersRoute from './routes/users.route';
import AuthRoute from './routes/auth.route';
import PaymentRoute from './routes/payment.route';
import TransactionsRoute from './routes/transactions.routes';
import validateEnv from './utils/validateEnv';

validateEnv();

const app = new App([
  new IndexRoute(),
  new UsersRoute(),
  new AuthRoute(),
  new PaymentRoute(),
  new TransactionsRoute()
]);

app.listen();
