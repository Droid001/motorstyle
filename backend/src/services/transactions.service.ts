import HttpException from '../exceptions/HttpException';
import { isEmptyObject } from '../utils/util';
import { Payment } from '../interfaces/payment.interface';
import { Transaction } from '../interfaces/transactions.interface';
import transactionsModel from '../models/transactions.model';

class TransactionsService {
    public transactions = transactionsModel;

    public async findTransactions(page: number = 1, linesQuantity: number = 10): Promise<any> {
        const transactions: Transaction[] = await this.transactions.find().sort({ 'date': -1 });

        let arr: Transaction[] = [];
        const pageFirstTransactionIndex: number = page * linesQuantity - linesQuantity;
        const lastIndex = pageFirstTransactionIndex + linesQuantity;
        const isLastPage = (transactions[lastIndex] == undefined);

        for (let i = pageFirstTransactionIndex; i < lastIndex; i++) {
            if (transactions[i] != undefined) {
                arr.push(transactions[i]);
            }
            else {
                break;
            }
        }
        return {
            transactionsList: arr,
            isLastPage: isLastPage
        };
    }

    public async findTransactionById(transactionId: string): Promise<Transaction> {
        const findTransaction: Transaction = await this.transactions.findById(transactionId);
        if (!findTransaction) throw new HttpException(409, "Transaction not found");

        return findTransaction;
    }

    public async createTransaction(transactionData: Transaction): Promise<Transaction> {
        if (isEmptyObject(transactionData)) throw new HttpException(400, "Uncorrect transaction data");

        const createTransactionData: Transaction = await this.transactions.create(transactionData);
        return createTransactionData;
    }

    public async setStatus(payment:Payment): Promise<void> {
        if (payment.transactionNumber == undefined) { throw new HttpException(400, "Uncorrect transaction data") };

        const filter: object = { transactionNumber: payment.transactionNumber }
        const update: object = { statusCode: payment.statusCode, status: payment.status }

        const transaction: Transaction = await this.transactions.findOneAndUpdate(filter, update)
    }
}

export default TransactionsService;