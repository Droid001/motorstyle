export interface Payment {
    transactionNumber: number,
    status:string,
    statusCode:number
  }
  