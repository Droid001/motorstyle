import { NextFunction, Request, Response } from 'express';
import paymentService from '../services/payment.sevice';
import transactionService from '../services/transactions.service';
import usersService from '../services/users.service'
import { Payment } from '../interfaces/payment.interface';
import { User } from 'interfaces/users.interface';


class PaymentController {
    public paymentService = new paymentService();
    public transactionService = new transactionService();
    public usersService = new usersService();

    public balance = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const cash: string = await this.paymentService.getBalance();
            res.json({ cash: cash })
        } catch (error) {
            next(error);
        }
    }

    public transaction = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const payment: Payment = await this.paymentService.transctionToCard(+req.body.summ, +req.body.accountNumber);
            const user: User = await this.usersService.findUserByRememberToken(req.body.token);

            this.transactionService.createTransaction({
                user:user.login,
                transactionNumber: payment.transactionNumber,
                summ: +req.body.summ,
                accountNumber: +req.body.accountNumber,
                status: payment.status,
                statusCode: +payment.statusCode,
                date: Date.now()
            })

            res.json(
                payment
            )
        } catch (error) {
            next(error);
        }
    }

    public status = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const payment: Payment = await this.paymentService.getStatus(+req.body.accountNumber, +req.body.transactionNumber);
            this.transactionService.setStatus(payment);
            res.json(
                payment
            )
        } catch (error) {
            next(error);
        }
    }
}

export default PaymentController;
