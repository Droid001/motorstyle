import { Request } from 'express';
import { User } from './users.interface';

export interface DataStoredInToken {
  rememberToken: string;
}

export interface TokenData {
  token: string;
  expiresIn: number;
}

export interface RequestWithUser extends Request {
  user: User;
}
