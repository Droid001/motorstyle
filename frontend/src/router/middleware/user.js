import store from "@/store";

export default async function user(to, from, next) {
    await store.dispatch("CHECK");
    const role = store.getters.GET_ROLE;
    if ((role == 'user') || (role == 'admin')) {
        next()
    } else {
        next('/login')
    }
}