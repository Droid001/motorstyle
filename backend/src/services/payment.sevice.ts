import axios from 'axios'
import { Payment } from "../interfaces/payment.interface";
const parser = require('xml2json');

class PaymentService {

    private qiwiApiURL = process.env.QIWI_API_URL;
    private qiwiLogin = process.env.QIWI_LOGIN;
    private qiwiPassword = process.env.QIWI_PASSWORD;

    public async getBalance(): Promise<string> {
        let requestBody: string = `
            <request>
            <request-type>ping</request-type>
            <terminal-id>${this.qiwiLogin}</terminal-id>
            <extra name="password">${this.qiwiPassword}</extra>
        </request>`


        return axios.post(this.qiwiApiURL, requestBody, {
            headers: {
                'Content-Type': 'text/xml; charset=utf-8'
            }
        })
            .then((response) => {
                return JSON.parse(parser.toJson(response.data)).response.balances.balance.$t;
            })
            .catch((e) => {
                throw e;
            })
    }

    public async transctionToCard(summ: number, accountNumber: number): Promise<Payment> {

        const transactionNumber: number = this.createTransactionNumber();

        const requestBody: string = `
        <request>
        <request-type>pay</request-type>
        <terminal-id>${this.qiwiLogin}</terminal-id>
        <extra name="password">${this.qiwiPassword}</extra>
        <auth>
            <payment>
                <transaction-number>${transactionNumber}</transaction-number>
                <from>
                    <ccy>RUB</ccy>
                </from>
                <to>
                    <amount>${summ}</amount>
                    <ccy>RUB</ccy>
                    <account-number>${accountNumber}</account-number>
                    <service-id>34020</service-id>
                </to>
            </payment>
        </auth>
        </request>`;

        return axios.post(this.qiwiApiURL, requestBody, {
            headers: {
                'Content-Type': 'text/xml; charset=utf-8'
            }
        })
            .then((response) => {
                const answerData = JSON.parse(parser.toJson(response.data));

                const payment: Payment = {
                    status: answerData.response.payment.message,
                    statusCode: +answerData.response.payment.status,
                    transactionNumber: transactionNumber
                }

                return payment;
            })
            .catch((e) => {
                throw e;
            })
    }

    public async getStatus(accountNumber: number, transactionNumber: number): Promise<Payment> {

        const requestBody: string = `
        <request>
          <request-type>pay</request-type>
          <extra name="password">${this.qiwiPassword}</extra>
          <terminal-id>${this.qiwiLogin}</terminal-id>
          <status>
            <payment>
              <transaction-number>${transactionNumber}</transaction-number>
              <to>
                <account-number>${accountNumber}</account-number>
              </to>
            </payment>
          </status>
        </request>`

        return axios.post(this.qiwiApiURL, requestBody, {
            headers: {
                'Content-Type': 'text/xml; charset=utf-8'
            }
        })
            .then((response) => {
                const answerData = JSON.parse(parser.toJson(response.data));

                const payment: Payment = {
                    status: answerData.response.payment.message,
                    statusCode: +answerData.response.payment.status,
                    transactionNumber: transactionNumber
                }

                return payment;
            })
            .catch((e) => {
                throw e;
            })
    }

    private createTransactionNumber() {
        return Math.round(Math.random() * 1000000000000);
    }
}

export default PaymentService;