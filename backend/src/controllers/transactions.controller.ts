import { NextFunction, Request, Response } from 'express';
import { Transaction } from '../interfaces/transactions.interface';
import transactionsService from '../services/transactions.service';

class TransactionssController {
  public transactionsService = new transactionsService();

  public getTransactions = async (req: Request, res: Response, next: NextFunction) => {
    try {

      const findTransactionsData: any = await this.transactionsService.findTransactions(+req.query.page, +req.query.linesQuantity);
      res.status(200).json(findTransactionsData);
    } catch (error) {
      next(error);
    }
  }
}

export default TransactionssController;
