const baseUrl = process.env.VUE_APP_API_URL;
import axios from "axios";

export function BALANCE(context) {
    const token = context.rootState.usersModule.user.token;
    axios
        .post(baseUrl + "/payment/balance", { token: token })
        .then(response => {
            context.commit("SET_BALANCE", response.data);
        })
        .catch(() => {
            context.commit(
                "SET_ALERT",
                "При проверке баланса счета прозошла ошибка. Пожалуйста попробуйте позже."
            );
        });
}

export function TRANSACTION(context, data) {
    const token = context.rootState.usersModule.user.token;
    data.token = token;
    axios
        .post(baseUrl + "/payment/transaction", data)
        .then(response => {
            context.commit("SET_ALERT", response.data.answer);
        })
        .catch(e => {
            context.commit("SET_ALERT", e.data);
        });
}

export function TRANSACTIONS(context, data) {
    const token = context.rootState.usersModule.user.token;
    axios
        .post(
            baseUrl +
            `/transactions?page=${data.page}&linesQuantity=${data.linesQuantity}`, { token: token }
        )
        .then(response => {
            if (response.data.isLastPage) {
                context.commit("SET_ALERT", "Последняя страница");
            }
            context.commit("SET_TRANSACTIONS", response.data);
        })
        .catch(() => {
            context.commit(
                "SET_ALERT",
                "При получении списка транзакций произошла ошибка"
            );
        });
}

export function TRANSACTION_STATUS(context, data) {
    const token = context.rootState.usersModule.user.token;
    data.token = token;
    axios
        .post(baseUrl + "/payment/status", data)
        .then(response => {
            context.commit("SET_TRANSACTION_STATUS", response.data);
        })
        .catch(() => {
            context.commit(
                "SET_ALERT",
                "При получении статуса транзакции произошла ошибка"
            );
        });
}