const baseUrl = process.env.VUE_APP_API_URL;
import axios from "axios";
import router from "@/router";

export function LOGIN(context, data) {
    axios
        .post(baseUrl + "/auth/login", data)
        .then(response => {
            context.commit("SET_AUTH_DATA", response.data);
            router.push("/");
        })
        .catch(e => {
            context.commit("SET_ALERT", e.response.data.message);
        });
}

export function CHECK(context) {
    const token = context.state.user.token;
    axios
        .post(baseUrl + "/auth/check", { token: token })
        .then(response => {
            context.commit("SET_AUTH_ROLE", response.data.role);
            if (response.data.role == 'guest') {
                router.push('/login')
            }
        })
        .catch(e => {
            context.commit("SET_ALERT", e.response.data.message);
            context.commit("SET_AUTH_DATA", {
                token: "",
                role: "guest",
                name: ''
            })
            router.push('/login')
        });
}

export function LOGOUT(context) {
    const token = context.state.user.token;
    axios
        .post(baseUrl + "/auth/logout", { token: token })
        .then(response => {
            context.commit("SET_ALERT", response.data.message);
            context.dispatch('CHECK');
        })
        .catch(e => {
            context.commit("SET_ALERT", e.response.data.message);
        });
}

//Admin routes

export function USERS(context) {
    const token = context.state.user.token;
    axios.post(baseUrl + '/users/get', { token: token })
        .then(response => {
            context.commit('SET_USERS', response.data.data)
        })
        .catch(e => {
            context.commit("SET_ALERT", e.response.data.message);
        });

}

export function DELETE_USER(context, id) {
    const token = context.state.user.token;
    axios.delete(baseUrl + '/users/' + id, { token: token })
        .then(response => {
            context.commit("SET_ALERT", response.data.message);
            context.dispatch('USERS');
        })
        .catch(e => {
            context.commit("SET_ALERT", e.response.data.message);
        });
}

export function CREATE_USER(context, data) {
    const token = context.state.user.token;
    data.token = token;
    axios.post(baseUrl + '/users', data)
        .then(response => {
            context.commit("SET_ALERT", response.data.message);
            context.dispatch('USERS');
        })
        .catch(e => {
            context.commit("SET_ALERT", e.response.data.message);
        })

}