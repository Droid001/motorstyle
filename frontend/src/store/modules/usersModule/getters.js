export function GET_USER(state) {
    return state.user;
}

export function GET_ROLE(state) {
    return state.user.role;
}

export function GET_USERS(state) {
    return state.users;
}