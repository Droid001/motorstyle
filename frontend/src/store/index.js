import Vue from "vue";
import Vuex from "vuex";


import createPersistedState from "vuex-persistedstate";
import SecureLS from "secure-ls";

const ls = new SecureLS({ isCompression: true });

import state from "./state";
import * as getters from "./getters";
import * as mutations from "./mutations";
import * as actions from "./actions";

import usersModule from "./modules/usersModule";
import paymentsModule from "./modules/paymentsModule";

Vue.use(Vuex);

export default new Vuex.Store({
    state: state,
    getters: getters,
    mutations: mutations,
    actions: actions,
    modules: {
        usersModule,
        paymentsModule
    },
    plugins: [createPersistedState({
        paths: ["usersModule.user"],
        storage: {
            getItem: key => ls.get(key),
            setItem: (key, value) => ls.set(key, value),
            removeItem: key => ls.remove(key)
        }
    })],
});