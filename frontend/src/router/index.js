import guest from './middleware/guest'
import user from './middleware/user'
import admin from './middleware/admin'

import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
const History = () =>
    import ("../views/History.vue");
const Admin = () =>
    import ("../views/Admin.vue");
const Login = () =>
    import ("../views/Login.vue");


Vue.use(VueRouter);

const routes = [{
        path: "/",
        name: "Home",
        component: Home,
        beforeEnter: user
    },
    {
        path: "/history",
        name: "History",
        component: History,
        beforeEnter: user
    },
    {
        path: "/admin",
        name: "Admin",
        component: Admin,
        beforeEnter: admin
    },
    {
        path: "/login",
        name: "Login",
        component: Login,
        beforeEnter: guest
    }
];

const router = new VueRouter({
    mode: "history",
    base: "/",
    routes
});

export default router;