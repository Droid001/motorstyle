import * as mongoose from 'mongoose';
import { Transaction } from '../interfaces/transactions.interface';

const transactionSchema = new mongoose.Schema({
    transactionNumber: String,
    summ: Number,
    accountNumber: Number,
    status: String,
    statusCode: Number,
    date: Number
});

const transactionModel = mongoose.model<Transaction & mongoose.Document>('Transaction', transactionSchema);

export default transactionModel;
