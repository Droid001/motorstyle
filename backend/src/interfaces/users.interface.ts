import {userRoles} from '../enums/users.enum';

export interface User {
  _id: string,
  login:string,
  email: string,
  password: string,
  name: string,
  role: string,
  rememberToken?:string
}