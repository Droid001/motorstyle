import { NextFunction, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import HttpException from '../exceptions/HttpException';
import { RequestWithUser } from '../interfaces/auth.interface';
import userModel from '../models/users.model';
import { User } from '../interfaces/users.interface'

async function authMiddleware(req: RequestWithUser, res: Response, next: NextFunction) {
    console.log(req.body)
    if (req.body.token !== undefined && req.body.token !== '') {
        const secret = process.env.JWT_SECRET;

        try {
            const token: string = req.body.token;
            const json: any = jwt.verify(token, secret) as string;
            const findUser: User = await userModel.findOne({ rememberToken: json.rememberToken });

            if (findUser) {
                if (findUser.role == 'admin') {
                    next();
                } else {
                    next(new HttpException(401, 'Wrong authentication token'));
                }
            } else {
                next(new HttpException(401, 'Wrong authentication token'));
            }
        } catch (error) {
            next(new HttpException(401, 'Wrong authentication token'));
        }
    } else {
        next(new HttpException(404, 'Authentication token missing'));
    }
}

export default authMiddleware;
