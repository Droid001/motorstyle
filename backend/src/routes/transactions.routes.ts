import { Router } from 'express';
import Route from '../interfaces/routes.interface';
import TransactionsController from '../controllers/transactions.controller';
import authMiddleware from '../middlewares/auth.middleware';


class TransactionsRoute implements Route {
    public path = '/transactions';
    public router = Router();
    public TransactionsController = new TransactionsController();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.post(`${this.path}`, authMiddleware, this.TransactionsController.getTransactions);
    }
}

export default TransactionsRoute;
