import { IsEmail, IsString } from 'class-validator';

export class CreateUserDto {
  @IsString()
  public login: string;

  @IsEmail()
  public email: string;

  @IsString()
  public password: string;

  @IsString()
  public name: string;

  @IsString()
  public role: string; 
}

export class CreateAuthUserDto {
  @IsString()
  public login: string;

  @IsString()
  public password: string;
}



