import { Router } from 'express';
import Route from '../interfaces/routes.interface';
import PaymentController from '../controllers/payment.controller';
import authMiddleware from '../middlewares/auth.middleware';

class PaymentRoute implements Route {
    public path = '/payment';
    public router = Router();
    public PaymentController = new PaymentController();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.post(`${this.path}/balance`, authMiddleware, this.PaymentController.balance);
        this.router.post(`${this.path}/transaction`, authMiddleware, this.PaymentController.transaction);
        this.router.post(`${this.path}/status`, authMiddleware, this.PaymentController.status);
    }
}

export default PaymentRoute;
