export function GET_BALANCE(state) {
  return state.balance;
}

export function GET_TRANSACTIONS_LIST(state) {
  return state.transactionsList;
}

export function TRANSACTIONS_LAST_PAGE(state) {
  return state.isLastPage;
}
